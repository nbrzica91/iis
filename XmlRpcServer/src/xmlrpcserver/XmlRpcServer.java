/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlrpcserver;

import java.io.IOException;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.XmlRpcServerConfigImpl;
import org.apache.xmlrpc.webserver.WebServer;

/**
 *
 * @author NelaBrzica
 */
public class XmlRpcServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws XmlRpcException, IOException {
        System.out.println("Pokrećem XML-RPC server...");
        
        WebServer server = new WebServer(8080);
        
        PropertyHandlerMapping phm = new PropertyHandlerMapping(); 
        phm.addHandler("Temperatura", Temperatura.class);
        server.getXmlRpcServer().setHandlerMapping(phm);
        
        XmlRpcServerConfigImpl serverConfig = (XmlRpcServerConfigImpl) server.getXmlRpcServer().getConfig();
        serverConfig.setEnabledForExtensions(true);      
        
        server.start();
        System.out.println("Uspješno pokrenut XML-RPC server!");
        System.out.println("Čekam zahtjeve...");
    }
    
}
