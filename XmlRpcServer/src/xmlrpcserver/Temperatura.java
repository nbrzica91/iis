/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlrpcserver;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author NelaBrzica
 */
public class Temperatura {

    public double GetTemperatura(String grad) throws MalformedURLException, IOException, ParserConfigurationException, SAXException {
        String url = "https://vrijeme.hr/hrvatska_n.xml";
        DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
        f.setNamespaceAware(false);
        DocumentBuilder b = f.newDocumentBuilder();
        URLConnection urlConnection = new URL(url).openConnection();
        urlConnection.addRequestProperty("Accept", "application/xml");
        Document doc = (Document) b.parse(urlConnection.getInputStream());
        Element rootElement = doc.getDocumentElement();
       
        NodeList list = rootElement.getElementsByTagName("Grad"); // list = listaGradova
        if (list != null && list.getLength() > 0) {
            for (int i = 0; i < list.getLength(); i++) {
                NodeList subList = list.item(i).getChildNodes(); //subList = lista svih podataka za jedan grad

                if (subList != null && subList.getLength() > 0) {
                    // subList.item(1) = čvor GradIme
                    // subList.item(1).getFirstChild().getNodeValue() = vrijednost čvora gradIme
                    if (subList.item(1).getFirstChild().getNodeValue().equals(grad)) {
                        // subList.item(7) = čvor Podatci
                        // subList.item(7).getChildNodes().item(1) = čvor Temp unutar čvora Podatci
                        return Double.parseDouble(subList.item(7).getChildNodes().item(1).getFirstChild().getNodeValue());
                    }
                }
            }
        }

        throw new IllegalArgumentException("Grad za zadani naziv nije pronađen");
    }
}
