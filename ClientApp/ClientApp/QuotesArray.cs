﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ClientApp
{
    [DataContract(Namespace = "http://schemas.datacontract.org/2004/07/RestAPI.Model")]
    public class QuotesArray
    {
        [DataMember(Order = 0)]
        public List<QuoteData> QuoteDataList { get; set; }

        public QuotesArray()
        {

        }

        public QuotesArray(List<QuoteData> quoteDataList)
        {
            QuoteDataList = quoteDataList;
        }
    }

    [DataContract(Namespace = "http://schemas.datacontract.org/2004/07/RestAPI.Model")]
    public class QuoteData
    {
        [DataMember(Order = 0)]
        public string Id { get; set; }

        [DataMember(Order = 1)]
        public string Type { get; set; }

        [DataMember(Order = 2)]
        public string Quote { get; set; }

        [DataMember(Order = 3)]
        public string Author { get; set; }

        public QuoteData()
        {

        }

        public QuoteData(string id, string type, string quote, string author)
        {
            Id = id;
            Type = type;
            Quote = quote;
            Author = author;
        }
    }
}
