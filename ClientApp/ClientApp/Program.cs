﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ClientApp
{
    class Program
    {
        static void Main(string[] args)
        {

            string validateWithXsd = "http://localhost:5000/api/Quotes/validate-xsd/";
            string validateWithRng = "http://localhost:5000/api/Quotes/validate-rng/";

            while (true)
            {
                Console.WriteLine("Molim vas odaberite jednu od ponuđenih opcija (a,b,c ili d):");
                Console.WriteLine("a. Validacija xsd");
                Console.WriteLine("b. Validacija rng");
                Console.WriteLine("c. Pretraga entiteta");
                Console.WriteLine("d. Povezivanje na javni REST API");
                Console.WriteLine("e. Validiraj s JAXB");

                string odgovor = Console.ReadLine();
                if (odgovor != "a" && odgovor != "b" && odgovor != "c" && odgovor != "d" && odgovor != "e")
                {
                    Console.WriteLine("Unijeli ste pogrešan znak. Molim vas pokušajte ponovo.");
                    
                }
                else
                {
                    switch (odgovor)
                    {
                        case "a":
                           kreiranjeZahtjeva(validateWithXsd);
                            break;
                        case "b":
                            kreiranjeZahtjeva(validateWithRng);
                            break;
                        case "c":
                            Console.WriteLine("Molim vas odaberite jedan tip izreke:");
                            Console.WriteLine("a. Life");
                            Console.WriteLine("b. Love");
                            Console.WriteLine("c. Success");
                            string odg = Console.ReadLine();
                            if (odg != "a" && odg!="b" && odg!="c")
                            {
                                Console.WriteLine("Unijeli ste pogrešan znak. Molim vas pokušajte ponovo.");
                                break;
                            }
                            if (odg == "a")
                            {
                                pozoviSOAP("Life");
                            }
                            else if (odg == "b")
                            {
                                pozoviSOAP("Love");
                            }
                            else if (odg == "c")
                            {
                                pozoviSOAP("Success");
                            }

                            break;
                        case "d":
                            callRapidApi().Wait();
                            break;
                        case "e":
                            callJavaServer();
                            break;
                        default:
                            break;
                    }

                }
            }
        }

        private static void callJavaServer()
        {
            TcpClient client = new TcpClient("localhost", 1133);
            string putanja = "C:\\Users\\NelaBrzica\\Documents\\Algebra\\TrecaGodina\\II.semestar\\IIS\\NelaBrzicaIIS\\quotesSOAP.xml";
            byte[] buf;
            buf = Encoding.UTF8.GetBytes(putanja + "\n");

            NetworkStream stream = client.GetStream();
            stream.Write(buf, 0, putanja.Length + 1);

            buf = new byte[100];
            stream.Read(buf, 0, 100);
            string odgovor = Encoding.UTF8.GetString(buf);
            Console.WriteLine(odgovor);
        }
 

        private static void pozoviSOAP(string odg)
        {
            QuoteService2.QuoteServiceSoapClient servis = new QuoteService2.QuoteServiceSoapClient(QuoteService2.QuoteServiceSoapClient.EndpointConfiguration.QuoteServiceSoap);

            QuotesArray q = new QuotesArray();
            List<QuoteData> qproba = servis.EntitySearchAsync(odg).Result.Body.EntitySearchResult
                .Select(x => new QuoteData
                {
                    Id = x.Id,
                    Type = x.Type,
                    Quote = x.Quote,
                    Author = x.Author,
                })
                .ToList();

            foreach (var item in qproba)
            {
                Console.WriteLine("ID: "+item.Id + " " + " Type: " + item.Type + " " + " Quote: " + item.Quote + " " + " Author: " + item.Author);
            }

        }

        private async static Task callRapidApi()
        {
            try
            {
                var client = new HttpClient();
                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri("https://get-quote1.p.rapidapi.com/api/quote/type/life"),
                    Headers =
                    {
                        { "x-rapidapi-key", "ad308e2ed9msh1a9b64674e2701cp18e80cjsn8900ccb0efbd" },
                        { "x-rapidapi-host", "get-quote1.p.rapidapi.com" },
                    },
                };


                var response = await client.SendAsync(request);
                
                response.EnsureSuccessStatusCode();
                var body = await response.Content.ReadAsStringAsync();
                Console.WriteLine(body);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }        
        }

        private static void kreiranjeZahtjeva(string path)
        {
            List<QuoteData> quoteData = new List<QuoteData>();
            QuoteData qd = new QuoteData();

            string id;
            string type;
            string quote;
            string author;
            string zelim;
            do
            {
                Console.WriteLine("Molim vas unesite id (max 6 znakova): ");
                id = Console.ReadLine();

                Console.WriteLine("Molim vas unesite tip citata (Life, Love ili Success): ");
                type = Console.ReadLine();

                Console.WriteLine("Molim vas unesite citat (max 200 znakova): ");
                quote = Console.ReadLine();

                Console.WriteLine("Molim vas unesite autora: ");
                author = Console.ReadLine();

                qd.Id = id;
                qd.Type = type;
                qd.Quote = quote;
                qd.Author = author;
                quoteData.Add(qd);

                Console.WriteLine("Želite li nastaviti (da/ne)?");
                zelim = Console.ReadLine();

            } while (zelim == "da");

            QuotesArray quotesArray = new QuotesArray(quoteData);

            DataContractSerializer serializer = new DataContractSerializer(typeof(QuotesArray));
            MemoryStream podaci = new MemoryStream();
            XmlWriter writer = XmlWriter.Create(podaci);
            serializer.WriteObject(writer, quotesArray);

            writer.Close();

            var podaciString = Encoding.UTF8.GetString(podaci.ToArray());
            byte[] nizBajtova = Encoding.UTF8.GetBytes(podaciString);
            HttpWebRequest zahtjev = (HttpWebRequest)WebRequest.Create(path);
            zahtjev.Accept = "application/xml";
            zahtjev.Method = HttpMethod.Post.ToString();
            zahtjev.ContentType = "application/xml";
            Stream podaciZahtjev = zahtjev.GetRequestStream();
            podaciZahtjev.Write(nizBajtova, 0, nizBajtova.Length);
            podaciZahtjev.Close();

            // slanje requesta i primanje odgovora
            HttpWebResponse odgovor = (HttpWebResponse)zahtjev.GetResponse();
            Stream podaciOdgovor = odgovor.GetResponseStream();
            DataContractSerializer deserijaliziraj = new DataContractSerializer(typeof(bool));
            bool uspjesnoDodano = (bool)deserijaliziraj.ReadObject(podaciOdgovor);

            if (uspjesnoDodano)
            {
                Console.WriteLine("Validacija je bila uspješna i izreke su dodane.");
            }
            else
            {
                Console.WriteLine("Validacija nije prošla i izreke nisu dodane.");
            }
        }
    }
}
