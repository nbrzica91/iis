﻿using Commons.Xml.Relaxng;
using Microsoft.AspNetCore.Mvc;
using RestAPI.Model;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace RestAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuotesController : ControllerBase
    {
        private bool greska = false;

        [HttpPost("validate-xsd")]
        public  bool ValidateWithXsd(XmlElement quotesArray)
        {
            try
            {
                XmlDocument docXML = quotesArray.OwnerDocument;
                docXML.AppendChild(quotesArray);
                
                XmlSchemaSet schema = new XmlSchemaSet();
                schema.Add("http://schemas.datacontract.org/2004/07/RestAPI.Model", Path.GetFullPath("quotes_shema.xsd"));
                XmlReader xmlReader = new XmlNodeReader(docXML);
                XDocument doc = XDocument.Load(xmlReader);

                doc.Validate(schema, (o, e) => {
                    Console.WriteLine("{0}", e.Message);
                    greska = true;
                }
                );

                if (!greska)
                {
                    DataContractSerializer deserijalizacija = new DataContractSerializer(typeof(QuotesArray));
                    MemoryStream xmlStream = new MemoryStream();
                    doc.Save(xmlStream);
                    xmlStream.Position = 0;
                    QuotesArray qa = (QuotesArray)deserijalizacija.ReadObject(xmlStream);

                    foreach (var item in qa.QuoteDataList)
                    {
                        Startup.QuotesArray.QuoteDataList.Add(item);
                    }
                
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }



        [HttpPost("validate-rng")]
        public bool ValidateWithRng(XmlElement quotesArray)
        {
            XmlDocument docXml = quotesArray.OwnerDocument;
            docXml.AppendChild(quotesArray);
            XmlReader xml = new XmlNodeReader(docXml);    
            XmlReader rng = XmlReader.Create(Path.GetFullPath("quotes_rng.rng"));
            using (var reader = new RelaxngValidatingReader(xml, rng))
            {
                reader.InvalidNodeFound += (source, message) =>
                {
                    Console.WriteLine("Error: " + message);
                    greska = true;
                    return true;
                };
                XDocument doc = XDocument.Load(reader);
            }

            if (greska)
            {
                return false;
            }
            else
            {
                DataContractSerializer deserijalizacija = new DataContractSerializer(typeof(QuotesArray));
                MemoryStream xmlStream = new MemoryStream();
                docXml.Save(xmlStream);
                xmlStream.Position = 0;
                QuotesArray qa = (QuotesArray)deserijalizacija.ReadObject(xmlStream);

                foreach (var item in qa.QuoteDataList)
                {
                    Startup.QuotesArray.QuoteDataList.Add(item);
                }
                return true;
            }
        }
    }
}