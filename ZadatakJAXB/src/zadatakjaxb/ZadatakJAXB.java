/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zadatakjaxb;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.datacontract.schemas._2004._07.soapservice.QuotesArray;
import org.xml.sax.SAXException;

/**
 *
 * @author NelaBrzica
 */
public class ZadatakJAXB {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws JAXBException, SAXException, IOException {
        
        ServerSocket serversocket = new ServerSocket(1133, 10);
        System.out.println("Server is ready ....");
        
        Socket client = serversocket.accept();
        Scanner scanner = new Scanner(client.getInputStream());
        PrintWriter pw = new PrintWriter(client.getOutputStream(), true);
        String xsdFile = "C:\\Users\\NelaBrzica\\Documents\\Algebra\\TrecaGodina\\II.semestar\\IIS\\NelaBrzicaIIS\\SOAPService\\quotes.xsd";
         
        String XmlFilename = scanner.nextLine();
        JAXBContext jaxbContext;
         
        try
        {
            //Get JAXBContext
            jaxbContext = JAXBContext.newInstance(QuotesArray.class);
             
            //Create Unmarshaller
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
             
            //Setup schema validator
            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema quotesSchema = sf.newSchema(new File(xsdFile));
            jaxbUnmarshaller.setSchema(quotesSchema);
             
            //Unmarshal xml file 
            QuotesArray quotes = (QuotesArray) jaxbUnmarshaller.unmarshal(new File(XmlFilename));
           
            System.out.println(quotes.getQuoteDataList().getQuoteData().toString());
            System.out.println("Xml fajl je ispravan");
             pw.println("Xml fajl je ispravan");
        
        }
        catch (JAXBException | SAXException e) 
        {
             System.out.println("Xml fajl nije ispravan");
              pw.println("Xml fajl nije ispravan");
        
             e.printStackTrace();
        }
        
          client.close();
    }
    
}
