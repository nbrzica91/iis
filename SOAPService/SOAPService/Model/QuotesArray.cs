﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SOAPService.Model
{
    [DataContract]
    class QuotesArray
    {
        [DataMember(Order = 0)]
        public List<QuoteData> QuoteDataList { get; set; }

        public QuotesArray()
        {

        }

        public QuotesArray(List<QuoteData> quoteDataList)
        {
            QuoteDataList = quoteDataList;
        }
    }

    [DataContract]
   public class QuoteData
    {
        [DataMember(Order = 0)]
        public string Id { get; set; }

        [DataMember(Order = 1)]
        public string Type { get; set; }

        [DataMember(Order = 2)]
        public string Quote { get; set; }

        [DataMember(Order = 3)]
        public string Author { get; set; }

        public QuoteData(string id, string type, string quote, string author)
        {
            Id = id;
            Type = type;
            Quote = quote;
            Author = author;
        }

        public QuoteData()
        {
        }
    }
}