﻿using SOAPService.Model;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Web.Services;
using System.Xml;

namespace SOAPService
{
    /// <summary>
    /// Summary description for QuoteService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class QuoteService : System.Web.Services.WebService
    {

        QuotesArray qa = new QuotesArray();
        List<QuoteData> quotes = new List<QuoteData>();
        QuoteData q1 = new QuoteData("abcde", "Love", "Loved you yesterday, love you still, always have, always will.", "Elaine Davis");
        QuoteData q2 = new QuoteData("fedcb", "Love", "Love is so short, forgetting is so long.", "Pablo Neruda");
        QuoteData q3 = new QuoteData("werdf", "Life", "The purpose of our lives is to be happy.", "Dalai Lama");
        QuoteData q4 = new QuoteData("wertz", "Success", "Everyone wants to be successful until they see what it actually takes.", "Ivo ivic");
     

        [WebMethod]
        public List<QuoteData> EntitySearch(string word)
        {
            quotes.Add(q1);
            quotes.Add(q2);
            quotes.Add(q3);
            quotes.Add(q4);

            qa.QuoteDataList = quotes;

            
            DataContractSerializer serializer = new DataContractSerializer(typeof(QuotesArray));
            string filePath = @"C:\Users\NelaBrzica\Documents\Algebra\TrecaGodina\II.semestar\IIS\NelaBrzicaIIS\quotesSOAP.xml";
            
            using (FileStream writer = new FileStream(filePath, FileMode.Create))
            {
                serializer.WriteObject(writer, qa);
                writer.Close();
            }

            XmlDocument document = new XmlDocument();
            document.Load(filePath);
            XmlNode root = document.DocumentElement;

            XmlNamespaceManager xmlnsManager = new XmlNamespaceManager(document.NameTable);
            xmlnsManager.AddNamespace("ns", "http://schemas.datacontract.org/2004/07/SOAPService.Model");

            XmlNodeList xnl = root.SelectNodes("/ns:QuotesArray/ns:QuoteDataList/ns:QuoteData[ns:Type='"+word+"']", xmlnsManager);

            List<QuoteData> result = new List<QuoteData>();
            for (int i = 0; i < xnl.Count; i++)
            {
                var currentNodeXml = "<QuoteData xmlns=\"http://schemas.datacontract.org/2004/07/SOAPService.Model\">"+xnl[i].InnerXml+"</QuoteData>";
                Stream ms = new MemoryStream(Encoding.UTF8.GetBytes(currentNodeXml));
          
                DataContractSerializer ds = new DataContractSerializer(typeof(QuoteData));
                result.Add((QuoteData)ds.ReadObject(ms));
            }

            return result;
        }
    }
}
