/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlrpcclient;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

/**
 *
 * @author NelaBrzica
 */
public class XmlRpcClient {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws MalformedURLException, XmlRpcException {
       
        XmlRpcClientConfigImpl konfiguracija = new XmlRpcClientConfigImpl();
        konfiguracija.setServerURL(new URL("http://localhost:8080"));
        
        org.apache.xmlrpc.client.XmlRpcClient klijent = new org.apache.xmlrpc.client.XmlRpcClient();
        klijent.setConfig(konfiguracija);
        
       /* System.out.println("Unesite naziv grada");
        Scanner sc= new Scanner(System.in);
        String grad= sc.nextLine();           
        Object[] parametri = new Object[]{grad};*/
        Object[] parametri = new Object[]{"Bjelovar"};
        double rezultat = (double)klijent.execute("Temperatura.GetTemperatura",parametri );
        System.out.println("Temperatura za zadani grad je " + rezultat);
    }
    
}
